Hi!!!

I will help you with *finding* and *downloading* songs from SoundCloud.

If you need to find some song, just send a song name or singer name.
For example: _Adele_ or _Lean On_

If you have a link to the song or playlist, I will send it to you without asking too much!
To practice, try this one: *https://soundcloud.com/uiceheidd/lucid-dreams-forget-me*

*P.S.* You can send all this text, and I will find song link for you!!!