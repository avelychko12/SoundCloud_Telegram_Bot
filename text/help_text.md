You can *find* a *track* by typing a search text.
Example: _My lovely song_

Or just enter text with a _link_ to the *track* or *playlist* from SoundCloud.
Like this: *https://soundcloud.com/uiceheidd/lucid-dreams-forget-me*

*P.S.* You can send all this text, and I will find song link for you!!!