from telegram import InlineKeyboardButton, InlineKeyboardMarkup


def get_inline_keyboard(origin: str, callback_data_list: list, page_num: int, has_next: bool,
                        nav_param: str) -> InlineKeyboardMarkup:
    navigation_part = []

    if page_num > 0:
        navigation_part.append(
            InlineKeyboardButton("⬅️", callback_data='%s nav %d %s' % (origin, page_num - 1, nav_param))
        )

    navigation_part.append(
        InlineKeyboardButton("Download All", callback_data='%s all %d %s' % (origin, page_num, nav_param))
    )

    if has_next:
        navigation_part.append(
            InlineKeyboardButton("➡️", callback_data='%s nav %d %s' % (origin, page_num + 1, nav_param))
        )

    keyboard = [
        [InlineKeyboardButton(str(i), callback_data='%s %s' % (origin, cd))
         for i, cd in enumerate(callback_data_list, 1)],

        navigation_part
    ]

    return InlineKeyboardMarkup(keyboard)
