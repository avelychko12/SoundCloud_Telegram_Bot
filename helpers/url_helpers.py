import re
from enum import Enum

import requests
from soundcloud import Client
from soundcloud.resource import Resource

import settings
from exceptions import ValidationError, SoundCloudError


class UrlType(Enum):
    SoundCloud = r'^http[s]?://(api.|m.|)soundcloud.com/'
    SoundCloudMobile = r'^http[s]?://m.soundcloud.com/'
    track = r'^https://api.soundcloud.com/tracks/\d+'
    playlist = r'^https://api.soundcloud.com/playlists/\d+'

    track_permalink = r'http[s]?://[m]?[.]?soundcloud.com/\S+/\S*\w'
    playlist_permalink = r'http[s]?://[m]?[.]?soundcloud.com/\S+/sets/\S*\w'


def get_permalink_urls_from_msg(text: str) -> (set, set):
    track_urls = set(re.findall(UrlType.track_permalink.value, text))
    playlist_urls = set(re.findall(UrlType.playlist_permalink.value, text))

    for url in playlist_urls:
        track_urls.remove(url)

    return track_urls, playlist_urls


def find_tracks(query: str, page_num: int) -> (list, bool):
    client = Client(client_id=settings.CLIENT_ID)

    tracks_per_page = 5
    tracks_info = client.get('/tracks', q=query, limit=tracks_per_page, linked_partitioning=1,
                             offset=tracks_per_page * page_num)

    return tracks_info.collection, tracks_info.next_href is not None


def get_resource(url: str, url_type: UrlType) -> Resource:
    client = Client(client_id=settings.CLIENT_ID)

    if _is_resource_uri(url):
        return _get_resource_by_uri(url)

    url = rebuild_mobile_link(url)

    try:
        resource = client.get('/resolve', url=url)
    except requests.exceptions.HTTPError as error:
        if error.response.status_code >= 500:
            raise SoundCloudError()

        raise ValidationError('We can\'t find this %s. Check your link!' % url_type.name.split('_')[0])

    if not check_url_type(resource.url, url_type):
        raise ValidationError('You should enter link to the %s!' % url_type.name.split('_')[0])

    return resource


def _is_resource_uri(url: str) -> bool:
    return check_url_type(url, UrlType.track) or check_url_type(url, UrlType.playlist)


def _get_resource_by_uri(uri: str) -> Resource:
    client = Client(client_id=settings.CLIENT_ID)

    url = '/' + '/'.join(uri.split('/')[-2:])

    try:
        resource = client.get(url)
    except requests.exceptions.HTTPError as error:
        if error.response.status_code >= 500:
            raise SoundCloudError()
        raise ValidationError('We can\'t find this resourse.')

    return resource


def check_url_type(url: str, url_type: UrlType) -> bool:
    return re.search(url_type.value, url) is not None


def rebuild_mobile_link(url: str) -> str:
    if re.search(url, UrlType.SoundCloudMobile.value) is not None:
        url.replace('m.', '', 1)
    return url
