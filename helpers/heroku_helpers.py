import os

import requests
from telegram import Bot
from telegram.ext import Updater, Job

import settings


def heroku_add_webhook(updater: Updater) -> Updater:
    PORT = int(os.environ.get('PORT', '8443'))
    updater.start_webhook(listen="0.0.0.0",
                          port=PORT,
                          url_path=settings.TOKEN)
    updater.bot.set_webhook(settings.HEROKU_URL + settings.TOKEN)

    return updater


def heroku_make_request(bot: Bot, job: Job):
    requests.get(settings.HEROKU_URL)
