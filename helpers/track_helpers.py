import os

import eyed3
from eyed3 import id3
import requests
from soundcloud import Client
from soundcloud.resource import Resource

import settings


def get_tmp_track_path(title: str, chat_id: int) -> str:
    user_tmp_dir = os.path.join(settings.TMP_DIR, str(chat_id))

    if not os.path.exists(user_tmp_dir):
        os.mkdir(user_tmp_dir)

    return os.path.join(user_tmp_dir, '%s.mp3' % title)


def download_track(track: Resource, track_path: str, chat_id: int):
    client = Client(client_id=settings.CLIENT_ID)
    stream_url = client.get(track.stream_url, allow_redirects=False).location

    req = requests.get(stream_url)

    with open(track_path, 'wb') as f:
        for chunk in req.iter_content(chunk_size=50000):
            f.write(chunk)

    _configure_id3_tags(track, track_path, chat_id)


def _configure_id3_tags(track: Resource, track_path: str, chat_id: int):
    artist = track.user['username']
    title = track.title

    audiofile = eyed3.load(track_path)
    audiofile.initTag()

    audiofile.tag.artist = artist
    audiofile.tag.title = title

    if track.genre:
        audiofile.tag.genre = id3.Genre(track.genre)

    if track.artwork_url:
        artwork_path = _download_artwork(track.artwork_url, chat_id)
        audiofile.tag.images.set(3, open(artwork_path, 'rb').read(), 'image/jpeg')
        os.remove(artwork_path)

    audiofile.tag.save()


def _download_artwork(url: str, chat_id: int) -> str:
    url = url.replace('large.jpg', 't500x500.jpg')
    path = os.path.join(settings.TMP_DIR, str(chat_id), url.split('/')[-1])

    req = requests.get(url)

    with open(path, 'wb') as f:
        for chunk in req.iter_content(chunk_size=50000):
            f.write(chunk)

    return path


def remove_track(path: str):
    os.remove(path)
