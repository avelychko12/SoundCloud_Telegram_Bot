import os

from telegram.ext import Updater

import settings
from handlers_configuration import add_handlers
from helpers.heroku_helpers import heroku_add_webhook, heroku_make_request


def get_configured_updater() -> Updater:
    updater = Updater(settings.TOKEN)

    updater = add_handlers(updater)

    if settings.IS_HEROKU:
        updater = heroku_add_webhook(updater)
        updater.job_queue.run_repeating(heroku_make_request, interval=60 * 29)

    return updater


def start_bot(updater: Updater):
    _create_dirs()
    updater.start_polling()
    updater.idle()


def _create_dirs():
    if not os.path.exists(settings.TMP_DIR):
        os.mkdir(settings.TMP_DIR)
    if not os.path.exists(settings.SONGS_DIR):
        os.mkdir(settings.SONGS_DIR)
