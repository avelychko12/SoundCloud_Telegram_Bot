![](https://upload.wikimedia.org/wikipedia/ru/thumb/9/92/SoundCloud_logo.svg/1280px-SoundCloud_logo.svg.png)

# SoundCloud Telegram Bot
This bot allows you to search and download music from SoundCloud!

Technologies used:
 - python-telegram-bot
 - SoundCloud API
 - eyeD3

[Link to bot](https://t.me/sound_cloud_downloader_bot)