import os

TOKEN = os.environ.get('BOT_TOKEN')
ADMIN = os.environ.get('ADMIN_USERNAME')
CLIENT_ID = os.environ.get('SC_CLIENT_ID')

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
TMP_DIR = os.path.join(BASE_DIR, 'tmp')
SONGS_DIR = os.path.join(BASE_DIR, 'songs')
TEXT_DIR = os.path.join(BASE_DIR, 'text')

IS_HEROKU = os.environ.get('IS_HEROKU') == 'True'
HEROKU_URL = os.environ.get('HEROKU_URL')

BOTAN_TOKEN = os.environ.get('BOTAN_TOKEN')

BOT_NAME = os.environ.get('BOT_NAME')
