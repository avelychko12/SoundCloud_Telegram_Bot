import logging
import os

from telegram import Bot, Update, ParseMode, ChatAction, TelegramError

import botan
import settings
from exceptions import BaseMessageError, ValidationError
from helpers.keyboard_helpers import get_inline_keyboard
from helpers.track_helpers import get_tmp_track_path, download_track, remove_track
from helpers.url_helpers import get_permalink_urls_from_msg, check_url_type, UrlType, get_resource, find_tracks

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def error_handler(bot: Bot, update: Update, error):
    if update and update.message:
        update.message.reply_text('*Oops* We have some problems. Please try again.')

    botan.track(settings.BOTAN_TOKEN, 'error', error.__dict__, 'ERROR')

    logger.warning('Update "%s" caused error "%s"', update, error)


def start_handler(bot: Bot, update: Update):
    botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), '/start')

    user_tmp_dir = os.path.join(settings.TMP_DIR, str(update.message.chat_id))

    if not os.path.exists(user_tmp_dir):
        os.mkdir(user_tmp_dir)

    with open(os.path.join(settings.TEXT_DIR, 'start_text.md')) as f:
        update.message.reply_text(f.read(), parse_mode=ParseMode.MARKDOWN)


def help_handler(bot: Bot, update: Update):
    botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), '/help')

    with open(os.path.join(settings.TEXT_DIR, 'help_text.md')) as f:
        update.message.reply_text(f.read(), parse_mode=ParseMode.MARKDOWN)


def message_handler(bot: Bot, update: Update):
    botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), 'Text')

    track_urls, playlist_urls = get_permalink_urls_from_msg(update.message.text)

    if len(track_urls) + len(playlist_urls) == 0:
        find_handler(bot, update, update.message.text.split())
        return

    for url in track_urls:
        track_handler(bot, update, [url])
    for url in playlist_urls:
        playlist_handler(bot, update, [url])


def find_handler(bot: Bot, update: Update, args: list):
    botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), '/find')

    search_query = ' '.join(args)

    page_num = 0
    tracks, has_next = find_tracks(search_query, page_num)

    if not tracks:
        update.message.reply_text('We haven\'t found any track')
        return

    reply_markup = get_inline_keyboard('find', [t.obj['uri'] for t in tracks], page_num, has_next, search_query)

    update.message.reply_text('\n'.join('%d) %s' % (i, t.obj['title']) for i, t in enumerate(tracks, 1)),
                              reply_markup=reply_markup)


def track_handler(bot: Bot, update: Update, args: list):
    chat_id = update.message.chat_id
    bot.send_chat_action(chat_id=chat_id, action=ChatAction.RECORD_AUDIO)

    try:
        permalink_url = _get_url_from_args(args)
    except ValidationError as error:
        botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), '/track: with not 1 arg')
        error.reply_user(update)
        return

    try:
        track = get_resource(permalink_url, UrlType.track)
    except BaseMessageError as e:
        e.reply_user(update)
        return

    track_path = get_tmp_track_path(track.title, chat_id)

    download_track(track, track_path, chat_id)

    bot.send_chat_action(chat_id=chat_id, action=ChatAction.UPLOAD_AUDIO)
    try:
        bot.send_audio(chat_id=chat_id, audio=open(track_path, 'rb'))
        botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), '/track')
    except TelegramError:
        botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), '/track:didn\'t send')
    finally:
        remove_track(track_path)


def playlist_handler(bot: Bot, update: Update, args: list):
    botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(), '/playlist')

    try:
        permalink_url = _get_url_from_args(args)
    except ValidationError as error:
        botan.track(settings.BOTAN_TOKEN, update.message.from_user, update.message.to_dict(),
                    '/playlist: with not 1 arg')
        error.reply_user(update)
        return

    try:
        playlist = get_resource(permalink_url, UrlType.playlist)
    except BaseMessageError as e:
        e.reply_user(update)
        return

    tracks = playlist.tracks
    tracks_urls = [t['uri'] for t in tracks]
    reply_markup = get_inline_keyboard('playlist', tracks_urls[:5], 0, len(tracks_urls) > 5, playlist.uri)

    update.message.reply_text('\n'.join('%d) %s' % (i, t['title']) for i, t in enumerate(tracks[:5], 1)),
                              reply_markup=reply_markup)


def button_query_callback(bot: Bot, update: Update):
    query = update.callback_query

    origin, *data = query.data.split()
    chat_id = query.message.chat_id
    message_id = query.message.message_id

    if len(data) == 1:
        track_handler(bot, query, [data[0]])
        return

    page_num = int(data[1])

    if origin == 'find':
        search_query = data[2]
        tracks, has_next = find_tracks(search_query, page_num)

        if data[0] == 'nav':
            if not tracks:
                bot.edit_message_text('We haven\'t found any track', chat_id, message_id)
                return

            reply_markup = get_inline_keyboard(origin, [t.obj['uri'] for t in tracks], page_num, has_next, search_query)

            page_num_text = 'Page %d\n' % (page_num + 1)
            tracks_text = '\n'.join('%d) %s' % (i, t.obj['title']) for i, t in enumerate(tracks, 1))
            text = page_num_text + tracks_text

            bot.edit_message_text(text, chat_id, message_id, reply_markup=reply_markup)

        elif data[0] == 'all':
            for track in tracks:
                track_handler(bot, query, [track.obj['uri']])
    elif origin == 'playlist':
        uri = data[2]

        playlist = get_resource(uri, UrlType.playlist)
        tracks = playlist.tracks
        tracks_count = len(tracks)
        tracks = tracks[page_num * 5: (page_num + 1) * 5]
        tracks_urls = [t['uri'] for t in tracks]

        if data[0] == 'nav':
            reply_markup = get_inline_keyboard(origin, tracks_urls, page_num, (page_num + 1) * 5 < tracks_count, uri)

            page_num_text = 'Page %d\n' % (page_num + 1)
            tracks_text = '\n'.join('%d) %s' % (i, t['title']) for i, t in enumerate(tracks, 1))
            text = page_num_text + tracks_text

            bot.edit_message_text(text, chat_id, message_id, reply_markup=reply_markup)

        elif data[0] == 'all':
            for url in tracks_urls:
                track_handler(bot, query, [url])


def _get_url_from_args(args):
    if len(args) != 1:
        raise ValidationError('You need to enter exactly 1 link!')

    if not check_url_type(args[0], UrlType.SoundCloud):
        raise ValidationError('You need to enter a link from the SoundCloud!')

    return args[0]
