import os
import sys
from threading import Thread

from telegram.ext import Updater, CommandHandler, Filters, MessageHandler, CallbackQueryHandler

import settings
from handlers import message_handler, start_handler, error_handler, find_handler, track_handler, playlist_handler, \
    button_query_callback, help_handler


def add_handlers(updater: Updater) -> Updater:
    updater = _configure_restart(updater, settings.ADMIN)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler('start', start_handler))
    dp.add_handler(CommandHandler('find', find_handler, pass_args=True))
    dp.add_handler(CommandHandler('track', track_handler, pass_args=True))
    dp.add_handler(CommandHandler('playlist', playlist_handler, pass_args=True))
    dp.add_handler(CallbackQueryHandler(button_query_callback))
    dp.add_handler(MessageHandler(Filters.text, message_handler))
    dp.add_handler(CommandHandler('help', help_handler))
    dp.add_error_handler(error_handler)

    return updater


def _configure_restart(updater: Updater, admin_username: str) -> Updater:
    def stop_and_restart():
        updater.stop()
        os.execl(sys.executable, sys.executable, *sys.argv)

    def restart(bot, update):
        update.message.reply_text('Bot is restarting...')
        Thread(target=stop_and_restart).start()

    updater.dispatcher.add_handler(CommandHandler('r', restart, filters=Filters.user(username=admin_username)))

    return updater
