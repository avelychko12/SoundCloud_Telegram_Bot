from helpers.bot_helpers import get_configured_updater, start_bot

if __name__ == '__main__':
    updater = get_configured_updater()
    start_bot(updater)
