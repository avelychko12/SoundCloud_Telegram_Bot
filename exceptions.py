class BaseMessageError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def reply_user(self, update):
        update.message.reply_text(self.msg)


class ValidationError(BaseMessageError):
    pass


class SoundCloudError(BaseMessageError):
    def __init__(self, msg='SoundCloud have some internal troubles'):
        super(SoundCloudError, self).__init__(msg)
